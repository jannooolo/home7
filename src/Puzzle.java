import java.util.*;

class Puzzle {

   private static String letters;
   private static String[] words;

   public static void main(String[] args) {
      //test 1
      solveWord("SEND","MORE","MONEY");

      //test2
      solveWord("COCA", "COLA", "OASIS");
      //test3

      solveWord("CROSS", "ROADS", "DANGER");

   }

   public static void arrange(int n, int k) {
      permute(n, k, new HashSet<Integer>(), new int[k]);
   }

   public static void permute(int n, int k, HashSet<Integer> set, int[] permutation) {
      if (set.size() == k)
         doPermute(n, k, set, permutation);
      else {
         for (int i=0; i<n; i++)
            if (!set.contains(i)) {
               permutation[set.size()] = i;
               set.add(i);
               permute(n,k,set,permutation);
               set.remove(i);
            }
      }
   }

   public static void doPermute(int n, int k, HashSet<Integer> set, int[] permutation) {
      HashMap<Character,Integer> getInt = new HashMap<Character,Integer>();
      for (int i=0; i<k; i++)
         getInt.put(letters.charAt(i), // "SENDMORY";
                 permutation[i]);
      int[] values = new int[3];
      for (int j=0; j<3; j++) {
         String word = words[j];
         if (getInt.get(word.charAt(0)) == 0)
            return;
         int value = 0;
         for (int i=0; i<word.length(); i++)
            value = 10*value + getInt.get(word.charAt(i));
         values[j] = value;
      }
      if (values[0] + values[1] == values[2]) {
         System.out.println( words[0] + " + " + words[1] + " = " + words[2]);
         System.out.println("   "+ words[0] + "\n" + " + " + words[1] + "\n" + " = " + words[2] + "\n" );
         System.out.println("Kasutatud tähed : " + letters);
         System.out.println("Tähtedele omastatud väärtused "+ getInt +"\n" + "arvutus tehe arvudega" );
         System.out.format("%8d\n+%7d\n=%7d\n",values[0],values[1],values[2]);
         System.out.println(""+"\n"+"--------------------------------------------------------------------------"+"\n");

      }
   }

   public static void solveWord(String word1, String word2, String word3) {
      words = new String[3];
      words[0] = word1;
      words[1] = word2;
      words[2] = word3;
      String all = word1+word2+word3;
      letters = "";
      for (int i=0; i<all.length(); i++) {
         char caracter = all.charAt(i);
         if (letters.indexOf(caracter) < 0) letters += caracter;
      }
      arrange(10,letters.length());
   }

}
//kasutatud materjal
// https://www.youtube.com/watch?v=HC6Y49iTg1k
// http://www.sw-engineering-candies.com/blog-1/howtosolvealphametricequationssendmoremoneywithconstraintprogramminginjava
// http://www.kosbie.net/cmu/fall-09/15-110/handouts/recursion/Cryptarithms.java
// https://docs.oracle.com/javase/7/docs/api/java/util/HashMap.html